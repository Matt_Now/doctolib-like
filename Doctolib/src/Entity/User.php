<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;



    #[ORM\Column(type: 'boolean')]
    private $isVerified = false;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $prenom = null;

    #[ORM\Column(length: 255)]
    private ?string $adresse = null;

    #[ORM\Column (nullable: true)]
    private ?bool $is_medecin = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nom_clinique = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $clinique_adresse = null;

    #[ORM\ManyToMany(targetEntity: Specialite::class, inversedBy: 'users')]
    private Collection $Specialite;

    #[ORM\ManyToMany(targetEntity: Commentaire::class, inversedBy: 'users')]
    private Collection $Commentaire;

    #[ORM\Column(nullable: true)]
    private ?int $favoris = null;

    #[ORM\Column (nullable: true)]
    private ?bool $rdv = null;

    #[ORM\ManyToOne(inversedBy: 'users')]
    private ?Rendezvous $Rendezvous = null;

    #[ORM\ManyToOne(inversedBy: 'users')]
    private ?Sexe $Sexe = null;

    public function __construct()
    {
        $this->Specialite = new ArrayCollection();
        $this->Commentaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function isIsMedecin(): ?bool
    {
        return $this->is_medecin;
    }

    public function setIsMedecin(bool $is_medecin): self
    {
        $this->is_medecin = $is_medecin;

        return $this;
    }

    public function getNomClinique(): ?string
    {
        return $this->nom_clinique;
    }

    public function setNomClinique(?string $nom_clinique): self
    {
        $this->nom_clinique = $nom_clinique;

        return $this;
    }

    public function getCliniqueAdresse(): ?string
    {
        return $this->clinique_adresse;
    }

    public function setCliniqueAdresse(?string $clinique_adresse): self
    {
        $this->clinique_adresse = $clinique_adresse;

        return $this;
    }

    /**
     * @return Collection<int, Specialite>
     */
    public function getSpecialite(): Collection
    {
        return $this->Specialite;
    }

    public function addSpecialite(Specialite $specialite): self
    {
        if (!$this->Specialite->contains($specialite)) {
            $this->Specialite->add($specialite);
        }

        return $this;
    }

    public function removeSpecialite(Specialite $specialite): self
    {
        $this->Specialite->removeElement($specialite);

        return $this;
    }

    /**
     * @return Collection<int, Commentaire>
     */
    public function getCommentaire(): Collection
    {
        return $this->Commentaire;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->Commentaire->contains($commentaire)) {
            $this->Commentaire->add($commentaire);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        $this->Commentaire->removeElement($commentaire);

        return $this;
    }

    public function __toString()
    {
        return $this->prenom;
    }

    public function getFavoris(): ?int
    {
        return $this->favoris;
    }

    public function setFavoris(?int $favoris): self
    {
        $this->favoris = $favoris;

        return $this;
    }

    public function isRdv(): ?bool
    {
        return $this->rdv;
    }

    public function setRdv(bool $rdv): self
    {
        $this->rdv = $rdv;

        return $this;
    }

    public function getRendezvous(): ?Rendezvous
    {
        return $this->Rendezvous;
    }

    public function setRendezvous(?Rendezvous $Rendezvous): self
    {
        $this->Rendezvous = $Rendezvous;

        return $this;
    }

    public function getSexe(): ?Sexe
    {
        return $this->Sexe;
    }

    public function setSexe(?Sexe $Sexe): self
    {
        $this->Sexe = $Sexe;

        return $this;
    }

}
