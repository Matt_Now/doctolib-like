<?php

namespace App\Entity;

use App\Repository\CliniqueRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CliniqueRepository::class)]
class Clinique
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $libelle = null;

    #[ORM\Column(length: 255)]
    private ?string $adressepostal = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getAdressepostal(): ?string
    {
        return $this->adressepostal;
    }

    public function setAdressepostal(string $adressepostal): self
    {
        $this->adressepostal = $adressepostal;

        return $this;
    }
}
