<?php

namespace App\Controller;

use App\Entity\Sousspecialite;
use App\Form\SousspecialiteType;
use App\Repository\SousspecialiteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/sousspecialite')]
class SousspecialiteController extends AbstractController
{
    #[Route('/', name: 'app_sousspecialite_index', methods: ['GET'])]
    public function index(SousspecialiteRepository $sousspecialiteRepository): Response
    {
        return $this->render('sousspecialite/index.html.twig', [
            'sousspecialites' => $sousspecialiteRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_sousspecialite_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SousspecialiteRepository $sousspecialiteRepository): Response
    {
        $sousspecialite = new Sousspecialite();
        $form = $this->createForm(SousspecialiteType::class, $sousspecialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sousspecialiteRepository->save($sousspecialite, true);

            return $this->redirectToRoute('app_sousspecialite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sousspecialite/new.html.twig', [
            'sousspecialite' => $sousspecialite,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_sousspecialite_show', methods: ['GET'])]
    public function show(Sousspecialite $sousspecialite): Response
    {
        return $this->render('sousspecialite/show.html.twig', [
            'sousspecialite' => $sousspecialite,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_sousspecialite_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Sousspecialite $sousspecialite, SousspecialiteRepository $sousspecialiteRepository): Response
    {
        $form = $this->createForm(SousspecialiteType::class, $sousspecialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sousspecialiteRepository->save($sousspecialite, true);

            return $this->redirectToRoute('app_sousspecialite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sousspecialite/edit.html.twig', [
            'sousspecialite' => $sousspecialite,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_sousspecialite_delete', methods: ['POST'])]
    public function delete(Request $request, Sousspecialite $sousspecialite, SousspecialiteRepository $sousspecialiteRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sousspecialite->getId(), $request->request->get('_token'))) {
            $sousspecialiteRepository->remove($sousspecialite, true);
        }

        return $this->redirectToRoute('app_sousspecialite_index', [], Response::HTTP_SEE_OTHER);
    }
}
