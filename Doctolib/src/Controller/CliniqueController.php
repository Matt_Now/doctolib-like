<?php

namespace App\Controller;

use App\Entity\Clinique;
use App\Form\CliniqueType;
use App\Repository\CliniqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/clinique')]
class CliniqueController extends AbstractController
{
    #[Route('/', name: 'app_clinique_index', methods: ['GET'])]
    public function index(CliniqueRepository $cliniqueRepository): Response
    {
        return $this->render('clinique/index.html.twig', [
            'cliniques' => $cliniqueRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_clinique_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CliniqueRepository $cliniqueRepository): Response
    {
        $clinique = new Clinique();
        $form = $this->createForm(CliniqueType::class, $clinique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cliniqueRepository->save($clinique, true);

            return $this->redirectToRoute('app_clinique_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('clinique/new.html.twig', [
            'clinique' => $clinique,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_clinique_show', methods: ['GET'])]
    public function show(Clinique $clinique): Response
    {
        return $this->render('clinique/show.html.twig', [
            'clinique' => $clinique,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_clinique_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Clinique $clinique, CliniqueRepository $cliniqueRepository): Response
    {
        $form = $this->createForm(CliniqueType::class, $clinique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cliniqueRepository->save($clinique, true);

            return $this->redirectToRoute('app_clinique_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('clinique/edit.html.twig', [
            'clinique' => $clinique,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_clinique_delete', methods: ['POST'])]
    public function delete(Request $request, Clinique $clinique, CliniqueRepository $cliniqueRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$clinique->getId(), $request->request->get('_token'))) {
            $cliniqueRepository->remove($clinique, true);
        }

        return $this->redirectToRoute('app_clinique_index', [], Response::HTTP_SEE_OTHER);
    }
}
