<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Specialite;
use App\Entity\User;
use App\Repository\SpecialiteRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(SpecialiteRepository $specialiteRepository, UserRepository $userRepository, Security $security): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'specialites' => $specialiteRepository->findAll(),
            'users' => $userRepository->findAll()
        ]);
    }

}
