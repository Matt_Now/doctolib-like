<?php

namespace App\Controller;

use App\Entity\Sexe;
use App\Form\SexeType;
use App\Repository\SexeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/sexe')]
class SexeController extends AbstractController
{
    #[Route('/', name: 'app_sexe_index', methods: ['GET'])]
    public function index(SexeRepository $sexeRepository): Response
    {
        return $this->render('sexe/index.html.twig', [
            'sexes' => $sexeRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_sexe_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SexeRepository $sexeRepository): Response
    {
        $sexe = new Sexe();
        $form = $this->createForm(SexeType::class, $sexe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sexeRepository->save($sexe, true);

            return $this->redirectToRoute('app_sexe_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sexe/new.html.twig', [
            'sexe' => $sexe,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_sexe_show', methods: ['GET'])]
    public function show(Sexe $sexe): Response
    {
        return $this->render('sexe/show.html.twig', [
            'sexe' => $sexe,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_sexe_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Sexe $sexe, SexeRepository $sexeRepository): Response
    {
        $form = $this->createForm(SexeType::class, $sexe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sexeRepository->save($sexe, true);

            return $this->redirectToRoute('app_sexe_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sexe/edit.html.twig', [
            'sexe' => $sexe,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_sexe_delete', methods: ['POST'])]
    public function delete(Request $request, Sexe $sexe, SexeRepository $sexeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sexe->getId(), $request->request->get('_token'))) {
            $sexeRepository->remove($sexe, true);
        }

        return $this->redirectToRoute('app_sexe_index', [], Response::HTTP_SEE_OTHER);
    }
}
