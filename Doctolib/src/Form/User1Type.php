<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class User1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('password')
            ->add('isVerified')
            ->add('nom')
            ->add('prenom')
            ->add('Sexe')
            ->add('adresse')
            ->add('is_medecin')
            ->add('nom_clinique')
            ->add('clinique_adresse')
            ->add('Specialite')
            ->add('Commentaire')
            ->add('favoris')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
