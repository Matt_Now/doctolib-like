<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Regex;


class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add ('nom',null, [
                'constraints'=> [
                    new Regex('/^[a-zA-Z]{3,}$/',
                    'Minimum 3 caractères')
                ]
                ])
                ->add ('prenom',null, [
                    'constraints'=> [
                        new Regex('/^[a-zA-Z]{3,}$/',
                        'Minimum 3 caractères')
                    ]
                    ])
            ->add ('Sexe')        
            ->add ('adresse')
            ->add('is_medecin', CheckboxType::class, [
                'required' => false,
                'data' => false,
            ])
            ->add('nom_clinique',null, [
                'required' => false,
                'constraints'=> [
                    new Regex('/^[a-zA-Z]{3,}$/',
                    'Minimum 3 caractères')
                ]
                ])
                ->add('clinique_adresse',null, [
                    'required' => false,
                    'constraints'=> [
                        new Regex('/^[a-zA-Z]{8,}$/',
                        'Minimum 8 caractères')
                    ]
                    ])
            ->add('email', null, [
                    'constraints' => [
                        new Regex('/^[a-zA-Z0-9._%+-]+@(outlook|gmail|yahoo)\.(com|fr)$/', 'L\'adresse e-mail n\'est pas valide')
                    ]
                    ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'constraints' => [
                new Regex('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/', 
                "Il faut un mot de passe de 8 caractères avec une majuscule, une minuscule, un chiffre et un caractère spécial.")
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
